{ config, lib, pkgs, ... }:

{
  services = {
    # Enable touchpad support (enabled by default in most desktop managers)
    libinput.enable = true;

  # Configuration options to optimize laptop performance
    auto-cpufreq.enable = true;
    auto-cpufreq.settings = {
      battery = {
        governor = "balanced";
        turbo = "never";
      };
      charger = {
        governor = "performance";
        turbo = "auto";
      };
    };
    # Conflict with auto-cpufreq
    power-profiles-daemon.enable = false;

    thermald.enable = true;
  };
}
