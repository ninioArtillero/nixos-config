# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  unstableTarball = fetchTarball
    # Points to nixpkgs-unstable last review;
    "https://github.com/nixos/nixpkgs/tarball/nixpkgs-unstable";
  # Specific commit (corresponding to either stable or unstable channel).
  # https://github.com/NixOS/nixpkgs/archive/ec750fd01963ab6b20ee1f0cb488754e8036d89d.tar.gz;
in {
  imports = [
    # Include the results of the hardware scan
    ./hardware-configuration.nix

    # Window manager. Uncomment only one at a time
    #./wm/xmonad.nix
    #./wm/plasma.nix
    ./wm/gnome.nix

    # Laptop optimization options
    ./laptop.nix

    # Realtime audio configuration
    #./realtime-audio.nix
  ];

  nixpkgs.config = {
    allowUnfree = true;
    # Unstable channel for bleeding-edge packages
    packageOverrides = pkgs: {
      unstable = import unstableTarball { config = config.nixpkgs.config; };
    };
  };

  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];

    # Binary Cache for Haskell.nix
    trusted-public-keys =
      [ "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" ];
    substituters = [ "https://cache.iog.io" ];

    # Persisten nix-shell derivations
    # https://nixos.wiki/wiki/Storage_optimization#Pinning
    keep-outputs = true;
    keep-derivations = true; # default: true
  };

  # Boot and kernel configuration
  boot = {
    # Grub
    loader.grub = {
      enable = true;
      device = "/dev/sda";
      useOSProber = false;
      # extraConfig = ;
    };

    # Text output
    initrd.verbose = false;
    consoleLogLevel = 0;
    # Splash screen
    plymouth.enable = true;
    #plymouth.theme = "breeze"; # NixOS branded variant
    kernelParams = [
      "quiet"
      "splash"
      "loglevel=3"
      "systemd.show_status=false"
      "udev.log_level=3"
    ];

    # Kernel
    kernelPackages = pkgs.linuxPackages_zen;
    kernel.sysctl = {
      # Some recomended values to optmize zram from the Arch Wiki
      "vm.swappiness" = 180; # default 60
      "vm.watermark_boost_factor" = 0;
      "vm.watermark_scale_factor" = 125;
      "vm.page-cluster" = 0;
      "vm.vfs_cache_pressure" = 100; # default 100
      "kernel.sysrq" = 244; # REISUB
    };

  };

  # Performance tweaks

  # Tune priorities and process attributes
  services.ananicy = {
    enable = true;
    package = pkgs.ananicy-cpp;
    rulesProvider = pkgs.ananicy-rules-cachyos;
  };

  # Profile-sync-daemon to store browser cache in RAM
  #services.psd = {
  #enable = true;
  #resyncTimer = "15min";
  #};
  # Fix insufficient space error
  #boot.runSize = "50%";

  # Virtual memory for swapping
  zramSwap.enable = true;

  networking.hostName = "Ryu"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Bluetooth
  hardware.bluetooth.enable = true;

  # Set your time zone.
  time.timeZone = "America/Mexico_City";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";
  i18n.extraLocaleSettings = {
    LC_TIME = "es_MX.UTF-8";
    LC_MONETARY = "es_MX.UTF-8";
    LC_PAPER = "es_MX.UTF-8";
    LC_NAME = "es_MX.UTF-8";
    LC_ADDRESS = "es_MX.UTF-8";
    LC_TELEPHONE = "es_MX.UTF-8";
    LC_MEASUREMENT = "es_MX.UTF-8";
    LC_IDENTIFICATION = "es_MX.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us,latam";
    xkb.variant = "";
    xkb.options = "grp:alt_caps_toggle";
  };

  # Configure console keymap
  console.keyMap = "la-latin1";

  # Fonts
  fonts.packages = with pkgs; [
    fira
    hasklig
    inconsolata-nerdfont
    libertine
    libertinus
  ];

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Audio setup
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.xavigo = {
    isNormalUser = true;
    description = "Xavier Góngora";
    extraGroups = [ "networkmanager" "wheel" "audio" ];
    packages = with pkgs; [ ];
  };

  users.users.karmio = {
    isNormalUser = true;
    description = "Victoria Karmín";
    extraGroups = [ "networkmanager" "audio" ];
    packages = with pkgs; [ ];
  };

  # Nix Store
  nix.settings.auto-optimise-store = true;

  # Shell Configuration
  programs.fish.enable = true;
  users.defaultUserShell = pkgs.fish;
  # Add shells to list
  environment.shells = with pkgs; [ fish ];
  environment.localBinInPath = true;

  # List packages installed in system profile. To search, run:
  # $ nix search <package>
  environment.systemPackages = with pkgs; [

    # TOOLS AND OFFICE
    enpass
    gimp
    gparted
    libreoffice
    obsidian
    pdfarranger
    pdfsam-basic
    qalculate-qt
    quarto
    texlive.combined.scheme-full
    texmaker
    ventoy
    xournalpp
    zotero

    # DICTIONARIES AND SPELLCHECKING
    hunspell
    hunspellDicts.es_MX
    hunspellDicts.en_US
    ispell

    # WEB
    brave
    floorp
    protonvpn-gui
    qbittorrent
    slack
    stremio
    thunderbird

    # MEDIA
    audacious
    calibre
    kdenlive
    mpv
    nomacs
    obs-studio

    # COMMUNICATION
    element-desktop
    #unstable.zoom-us
    zoom-us

    # MUSIC PRODUCTION, SOUND DESIGN and DSP
    audacity
    fluidsynth
    jamesdsp
    puredata
    qpwgraph
    reaper
    sonic-pi
    sonic-visualiser
    soundfont-fluid
    supercollider-with-plugins
    yabridge
    yabridgectl

    # NIX TOOLS
    niv
    nixfmt-classic # Doom Nix
    nix-prefetch-git

    # PROGRAMMING LANGUAGES
    ## Haskell
    cabal-install
    ghc
    ghcid
    haskell-language-server
    haskellPackages.hoogle
    stack
    cabal2nix
    ## JavaScript
    nodejs
    nodePackages.npm
    ## Scala
    sbt
    scala
    scalafmt
    ## Standard ML
    smlnj
    smlfmt
    ## Others
    lean4
    perl
    python3
    ruby

    # CLI
    curl # transfer files from urls
    cyme # modern lsusb
    direnv # read custom project environment from ./.envrc
    dmidecode # system hardware information from BIOS
    fd # Alternative to find, Doom optional dependency
    fastfetch # fancy terminal
    gh # Github
    ghostscript # pdf manipulation
    git # version control system
    python312Packages.grip # Markdown preview
    htop # system monitoring
    imagemagick # image manipulation
    inxi # system information
    killall # kill processes by name
    lazygit # LazyNvim
    lm_sensors # temperatur monitoring
    pandoc # conversion between document formats
    pdfgrep # search pdfs
    rclone # cloud backup utility
    ripgrep # Doom dependency
    rmtrash # move to trash when using rm
    shellcheck # Doom Shell
    shfmt # Doom Shell
    smartmontools # tools for monitoring hard drive health
    stow # symlink farm manager to admin dotfiles from a VCS
    tlrc # too-long-didnt-read client written in Rust
    unrar # RAR archives
    unzip # ZIP archives
    wget # retrive files from the web
    xclip # copy from stout to clipboard
    xorg.xkill # kill unresponsive application window
    z3 # Theorem prover and SMT solver
    zip # ZIP archives
    zoxide # convenient alternative to gnu cd command

    # SYSTEM
    alsa-lib
    autoconf # A GNU tool for automatically configuring source code
    automake # A GNU tool for automatically creating Makefiles
    coreutils-full # The basic utilities of the GNU operating system
    clang # Doom optional dependency
    cmake
    exfat # Exfat implementation
    exfatprogs # Userspace utilities
    gcc
    glibc
    gmp
    gnumake # make
    gnupg
    libcxx
    libtool # GNU Libtool, a generic library support script
    ncurses
    nixos-bgrt-plymouth
    xz # compression format
    zlib
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Emacs
  services.emacs = {
    package = pkgs.emacs;
    enable = true;
    defaultEditor = false;
  };

  # Neovim
  programs.neovim = {
    #package = pkgs.unstable.neovim-unwrapped;
    package = pkgs.neovim-unwrapped;
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
  };

  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
