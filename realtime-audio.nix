{ config, lib, pkgs, ... }:

{
  # Custom real-time audio configuration
  #
  # Reference: Nixos Wiki and musix
  # NOTE: I don't use musnix because setting vm.swappiness to 10 seems outdated
  # and not a good default.
  # Also, the performance cpu governor is bad for laptops.

  # Realtime privileges
  boot.kernelParams = [ "threadirqs" ];

  security.pam.loginLimits = [
    {
      domain = "@audio";
      item = "memlock";
      type = "-";
      value = "unlimited";
    }
    {
      domain = "@audio";
      item = "rtprio";
      type = "-";
      value = "99";
    }
    {
      domain = "@audio";
      item = "nofile";
      type = "soft";
      value = "99999";
    }
    {
      domain = "@audio";
      item = "nofile";
      type = "hard";
      value = "99999";
    }
  ];

  # Set udev rules
  #services.udev = {
    #extraRules = ''
      #KERNEL=="rtc0", GROUP="audio"
      #KERNEL=="hpet", GROUP="audio"
      #DEVPATH=="/devices/virtual/misc/cpu_dma_latency", OWNER="root", GROUP="audio", MODE="0660"
    #'';
  #};

  # Low-latency setup
  # Documentation on this and other options can be found int
  # https://nixos.wiki/wiki/PipeWire
  #services.pipewire.extraConfig.pipewire."92-low-latency" = {
  #  "context.properties" = {
  #    "default.clock.rate" = 48000;
  #    "default.clock.quantum" = 32;
  #    "default.clock.min-quantum" = 32;
  #    "default.clock.max-quantum" = 32;
  #  };
  #};
}
