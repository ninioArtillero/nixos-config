{ config, lib, pkgs, ... }:

{
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };

  # For systray icons, which also need systemPackages.gnomeExtensions.appindicator
  services.udev.packages = [ pkgs.gnome-settings-daemon ];

  environment = {

    # Must be manually set in the Extensions app
    systemPackages = with pkgs.gnomeExtensions; [
      appindicator # systray icons
      blur-my-shell
      #pop-shell # tiling windows
      quick-settings-tweaker
      unite # top panel tweaks
      yakuake # drop-down terminal
      pkgs.kdePackages.yakuake # For Yakuake entension
      pkgs.kdePackages.konsole # For Yakuake entension
      zilence # turn off notifications on Zoom screen share
    ];

    gnome.excludePackages = with pkgs; [
      epiphany # browser -> floorp/brave
      geary # email client -> thunderbird
      gnome-connections # remote desktops
      gnome-contacts
      gnome-tour
      gnome-weather
      loupe # image viewer -> nomacs
      totem # video player -> mpv
    ];
  };
}
