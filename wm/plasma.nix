# Configuración para NixOS de KDE PLASMA 5
# Esta configuración se importa desde `configuration.nix`

{ config, lib, pkgs, ... }:

{
  services = {

    # Enable the KDE Plasma Desktop Environment.
    desktopManager.plasma6.enable = true;
    displayManager.sddm.wayland.enable = true;
    displayManager.sddm.enable = true;

    # Enable the X11 windowing system.
    xserver. enable= true;
    # Switch default display server
    #services.displayManager.defaultSession = "plasmax11";

  };

  environment = {
    systemPackages = with pkgs.kdePackages; [
      applet-window-buttons6 # widget to hold maximized app buttons
      partitionmanager
      qtstyleplugin-kvantum # themes
      spectacle # screenshots
      yakuake # drop-down terminal
      okular # document viewer
    ];

    plasma6.excludePackages = with pkgs.kdePackages; [
      elisa # music player
      gwenview # image viewer
      kwrited # text editor
      oxygen # application style
    ];
  };
}
