{ config, lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
  
  alacritty
  dmenu

  ]

  services = {
    gnome3.gnome-keyring.enable = true;
    upower.enable = true;

    dbus = {
      enable = true;
      socketActivated = true;
      packages = [ pkgs.gnome3.dconf ];
    };

    xserver = {
      enable = true;
      startDbusSession = true;
      layout = "us";
      
      # Enable touchpad support (enabled default in most desktopManager).
      libinput = {
        enable = true;
        disableWhileTyping = true;
      };

      displayManager.defaultSession = "none+xmonad";

      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
	extraPackages = hpkgs: [
	  hpkgs.xmobar
	  hpkgs.xmonad-screenshot
      };
    };

  };

  services.blueman.enable = true;

  systemd.services.upower.enable = true;
}
